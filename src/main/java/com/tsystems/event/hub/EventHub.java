package com.tsystems.event.hub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EventHub
{
    public static void main(String[] args)
    {
        SpringApplication.run(EventHub.class, args);
    }
}
