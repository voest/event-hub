package com.tsystems.event.hub.controller;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class SocketHandler extends TextWebSocketHandler {

    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws InterruptedException, IOException 
    {
        // no action here
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception 
    {
        // the messages will be broadcasted to all users.
        sessions.add(session);
    }
    
    public void sendTextMessage(TextMessage message)
    {
        for (WebSocketSession webSocketSession : sessions) 
        {
            try 
            {
                webSocketSession.sendMessage(message);
            } 
            catch (Exception e) 
            {
                try 
                {
                    webSocketSession.close();
                } 
                catch (IOException e1) 
                {
                    // ignore exceptions
                }
                sessions.remove(webSocketSession);
            }
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception 
    {
        super.afterConnectionClosed(session, status);
        
        session.close(status);
        sessions.remove(session);
    }
}
