package com.tsystems.event.hub.controller;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.event.hub.model.Event;
import com.tsystems.event.hub.model.QuuppaEvent;
import com.tsystems.event.hub.model.QuuppaZone;

@RestController
public class EventHubController {

    private final static String ZONE_IDENTIFIER = "Zone";

    // Caches last device events 
    private ConcurrentMap<String, Event> lastEvent = new ConcurrentHashMap<String, Event>();
    private ConcurrentMap<String, Event> newEvent = new ConcurrentHashMap<String, Event>();
    private Boolean pushEventInProgress = new Boolean(false);

    // JSON object mapper
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private SocketHandler socketHandler;

    @RequestMapping(value = "/events", method = RequestMethod.POST)
    ResponseEntity<?> addEvent(@RequestBody QuuppaEvent quuppaEvent) {

        Event event = mapQuuppaEventToEvent(quuppaEvent);

        // Ignore events from unknown/anonymous sources
        // Ignore old events
        String source = event.getSource();
        if (source != null && !source.isEmpty()) {
            newEvent.compute(source, (k, v) -> v != null && v.getTimestamp() > event.getTimestamp() ? v : event);
            
            synchronized (pushEventInProgress) {
                if (!pushEventInProgress) {
                    pushEventInProgress = true;
                    pushEvent();
                }
            }
        }

        return ResponseEntity.noContent().build();
    }

    @Async
    public void pushEvent() {
        try {
            while (!newEvent.isEmpty()) {
                Set<String> sources = newEvent.keySet();
                lastEvent.putAll(newEvent);
                
                sources.forEach(source -> {
                    try {
                        byte[] eventAsBytes = objectMapper.writeValueAsBytes(lastEvent.get(source));
                        socketHandler.sendTextMessage(new TextMessage(eventAsBytes));
                    } catch (JsonProcessingException e) {
                        // Ignore malformed events
                    }
                    
                    newEvent.remove(source, lastEvent.get(source));
                });
            }
        }
        finally {
            synchronized (pushEventInProgress) {
                pushEventInProgress = false;
            }
        }
    }
    
    @RequestMapping(value = "/lastevents", method = RequestMethod.GET)
    public Collection<Event> lastEvents() {
        return lastEvent.values();
    }

    @RequestMapping(value = "/{source}/lastevent", method = RequestMethod.GET)
    public Event lastEvent(@PathVariable String source) {
        return lastEvent.get(source);
    }

    private Event mapQuuppaEventToEvent(QuuppaEvent quuppaEvent) {
        Event event = new Event();
        event.setSource(quuppaEvent.getName());
        event.setTimestamp(quuppaEvent.getLastPacketTS());

        // Find the matching zone
        String zoneName = "Unknown";
        if (quuppaEvent.getZones() != null) {
            List<QuuppaZone> appZones = quuppaEvent.getZones().stream()
                    .filter(zone -> zone.getName().startsWith(ZONE_IDENTIFIER))
                    .collect(Collectors.toList());

            if (appZones.size() == 0) {
                zoneName = String.format("No match. Source event zones: %s", quuppaEvent.getZones().stream().map(zone -> zone.getName()).collect(Collectors.toList()));
            } else if (appZones.size() == 1) {
                zoneName = appZones.get(0).getName();
            } else {
                zoneName = String.format("Multiple matches, one of %s", appZones.stream().map(zone -> zone.getName()).collect(Collectors.toList()));
            }
        }
        event.setZone(zoneName);

        // Alarm
        event.setAlarm(quuppaEvent.isShock());

        return event;
    }
}
