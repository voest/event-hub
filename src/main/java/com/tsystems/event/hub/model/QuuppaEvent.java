package com.tsystems.event.hub.model;

import java.util.List;

public class QuuppaEvent {

    private String name;
    private String id;
    private List<QuuppaZone> zones;
    private List<Integer> acceleration;
    private boolean shock;
    private long lastPacketTS;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<QuuppaZone> getZones() {
        return zones;
    }

    public void setZones(List<QuuppaZone> zones) {
        this.zones = zones;
    }

    public List<Integer> getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(List<Integer> acceleration) {
        this.acceleration = acceleration;
    }

    public boolean isShock() {
        return shock;
    }

    public void setShock(boolean shock) {
        this.shock = shock;
    }

    public long getLastPacketTS() {
        return lastPacketTS;
    }

    public void setLastPacketTS(long timestamp) {
        this.lastPacketTS = timestamp;
    }
}
