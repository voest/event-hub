package com.tsystems.event.hub.model;

public class Position
{
    private float latitude;
    private float longitude;
    private float altitude;
    private float accuracy; // accuracy, meters

    public float getLatitude()
    {
        return latitude;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLongitude()
    {
        return longitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }
    
    public float getAccuracy()
    {
        return accuracy;
    }

    public void setAccuracy(float acc)
    {
        this.accuracy = acc;
    }

    @Override
    public String toString()
    {
        return "Latitude=" + latitude + ", longitude=" + longitude + ", accuracy=" + accuracy;
    }
}
