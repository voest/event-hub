package com.tsystems.event.hub.model;

public class Event
{
    // mandatory 
    private String source; // event source UUID
    private long timestamp; // original recording stamp, UTC w/o time saving, milliseconds since 01.01.1970
    
    // optional 
    private Position position;
    private String zone;
    private boolean alarm;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(long ts)
    {
        this.timestamp = ts;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String toString()
    {
//        return "Timestamp=" + timestamp + ", latitude=" + latitude + ", longitude=" + longitude + ", accuracy=" + accuracy + ", zone=" + zone;
        return "Timestamp=" + timestamp + ", position=" + position + ", zone=" + zone;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

}
